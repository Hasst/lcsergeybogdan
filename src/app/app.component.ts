import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'lcSergeyBogdan';
  profileForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]+$'), Validators.minLength(6), Validators.maxLength(10)]],
      gender: ['', Validators.required],
      country: ['', Validators.required],
      city: ['', Validators.minLength(3)],
      address: ['', Validators.minLength(3)],
      additionalInfo: ''
    });
  }

  submit() {
    console.log(this.profileForm.value);
  }
}
